﻿using System;

namespace Algorithm
{
    public static class Summer
    {
        public static int Sum(int number1, int number2)
        {
            return number1 + number2;
        }
    }
}
